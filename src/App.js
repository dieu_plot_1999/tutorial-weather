import React from "react";
import {Box, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import TextField from "@mui/material/TextField";
import SearchIcon from "@mui/icons-material/Search";
import {IconButton} from "@mui/material";
import {InputAdornment} from "@mui/material";
import {useState} from "react";
import {useEffect} from "react";
import axios from "axios";
import Weather from "./components/Weather";
import Addittional from "./components/Additional";
import Main from "./components/Main";
import {getReportByCity} from "./apis";
import SearchBar from "./components/Search/SearchBar";
import CountryData from "./Data.json";

const useStyles = makeStyles({
  title: {
    color: "#2f3679",
    textAlign: "center",
    fontSize: "2rem!important",
    fontWeight: "600!important",
    background: "linear-gradient(90deg, #FDBB2D 0%, #22C1C3 100%)",
    borderRadius: 2,
  },
  textfield: {
    marginLeft: 15,
    marginTop: 250,
    color: "red",
  },
  Addittional: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: "35px",
    fontSize: "25px",
    color: "white",
  },
  painLine: {
    height: 2,
    width: 400,
    backgroundColor: "red",
  },
});

function App() {
  const classes = useStyles();
  const [city, setCity] = useState("ho chi minh");
  const [data, setData] = useState();
  console.log("city", getReportByCity(city));
  console.log("coundatatry", data);

  const [country, setCountry] = useState([]);
  console.log("country", country);

  useEffect(() => {
    getReportByCity(city).then((res) => {
      console.log("res", res);
      console.log("111111", setData(res.data));
    });
    axios();
  }, [city]);

  useEffect(() => {
    axios
      .get(
        "https://pkgstore.datahub.io/core/world-cities/world-cities_json/data/5b3dd46ad10990bca47b04b4739a02ba/world-cities_json.json"
      )
      .then((ress) => {
        setCountry(ress.data);
        console.log("res", ress.data);
      })
      .catch((err) => {
        console.log("err", err);
      });
  }, []);

  const datacountry = [...new Set(country.map((item) => item.country))];
  console.log("datacountry", datacountry);
  let states = country.filter((state) => state.country === "Vietnam");
  console.log("states", states);
  let subcountrys = [...new Set(states.map((item) => item.subcountry))];
  console.log("subcountrys", subcountrys);

  const handleCityChange = (e) => {
    setCity(e.target.value);
  };

  const handleSubmit = () => {
    console.log("hello");
  };

  return (
    <>
      <Box
        className={classes.root}
        sx={{
          width: 400,
          height: 650,
          marginLeft: 50,
          marginTop: 5,
          borderRadius: 2,
          boxShadow: 1,
          backgroundImage: `url(${"https://img5.thuthuatphanmem.vn/uploads/2021/12/28/anh-bau-troi-may-den-u-am-tuyet-vong_022428259.jpg"})`,
        }}
      >
        <Typography className={classes.title}>Weather</Typography>
        {/* <TextField
          sx={{
            marginTop: 5,
            marginLeft: 5,
            width: 330,
          }}
          value={city}
          onChange={handleCityChange}
        >
          </> */}
        <SearchBar
          placeholder="Enter a Book Name..."
          setCity={setCity}
          data={states}
        ></SearchBar>
        <Box
          sx={{
            width: 300,
            height: 300,
            marginLeft: 6,
            marginTop: 8,
          }}
        >
          <Main data={data}></Main>
          <Typography mt={5} ml={-6}></Typography>
          <div>
            <Addittional data={data}></Addittional>
          </div>
        </Box>
      </Box>
      <Weather data={data?.weather?.[0].icon}></Weather>
      <SearchBar placeholder="Enter a Book Name..." data={states}></SearchBar>
    </>
  );
}

export default App;
