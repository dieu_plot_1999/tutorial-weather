import axios from "axios";

const APP_ID = "7919a8049517fd070f6ac8814c5c3097";

export const getReportByCity = (city) =>
  axios.get(
    `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${APP_ID}&units=metric&lang=vi`
  ) || [];
