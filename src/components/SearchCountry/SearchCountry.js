import React from "react";
import TextField from "@mui/material/TextField";
import SearchIcon from "@mui/icons-material/Search";
import {IconButton} from "@mui/material";
import {InputAdornment} from "@mui/material";

export default function SearchCountry() {
  return (
    <>
      <TextField
        sx={{
          marginTop: 0,
          marginLeft: 5,
          width: 330,
        }}
        label="Search Enter..."
        InputProps={{
          endAdornment: (
            <InputAdornment>
              <IconButton>
                <SearchIcon />
              </IconButton>
            </InputAdornment>
          ),
        }}
        // onChange={handleOnChange}
      />
    </>
  );
}
