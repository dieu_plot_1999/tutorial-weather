import React from "react";
import {makeStyles} from "@mui/styles";
import {Card, Typography} from "@mui/material";

const DEFAULT_VALUE = "--";
export default function Main({data}) {
  let img = `http://openweathermap.org/img/wn/${data?.weather?.[0].icon}@2x.png`;
  return (
    <>
      <Typography
        sx={{
          align: "justifyContent",
          fontSize: "1.5rem",
          textAlign: "center",
          fontWeight: 600,
          color: "black",
        }}
      >
        {data?.name || DEFAULT_VALUE}
      </Typography>
      <Typography
        sx={{
          align: "justifyContent",
          fontSize: "1rem",
          textAlign: "center",
          fontWeight: 300,
          color: "black",
        }}
      >
        {data?.weather?.[0].description || DEFAULT_VALUE}
      </Typography>

      <img
        src={img}
        alt=""
        style={{
          justifyItems: "center",
          marginLeft: "100px",
          fontSize: "small",
          textAlign: "center",
        }}
      />

      <Typography
        sx={{
          marginTop: 3,
          marginLeft: 12,
          fontSize: "3rem",
          color: "black",
        }}
      >
        {Math.round(data?.main?.temp)}°C
      </Typography>
    </>
  );
}
