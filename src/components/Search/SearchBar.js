import React, {useState} from "react";
import "./SearchBar.css";
import SearchIcon from "@mui/icons-material/Search";
import CloseIcon from "@mui/icons-material/Close";

let flag = true;
function SearchBar({placeholder, setCity, data}) {
  const [filteredData, setFilteredData] = useState([]);
  const [wordEntered, setWordEntered] = useState("");
  const handleFilter = (event) => {
    flag = true;
    const searchWord = event.target.value;
    setWordEntered(searchWord);
    const newFilter = data.filter((value) => {
      return value.name.toLowerCase().includes(searchWord.toLowerCase());
    });

    if (searchWord === "") {
      flag = false;
      setFilteredData([]);
    }
    setFilteredData(newFilter);
  };

  const clearInput = () => {
    flag = true;
    setFilteredData([]);
    setWordEntered("");
  };

  const handleClick = (data) => {
    setWordEntered(data);
    flag = false;
  };
  return (
    <>
      <div className="search">
        <div className="searchInputs">
          <input
            type="text"
            placeholder={placeholder}
            value={wordEntered}
            onChange={handleFilter}
          />
          <div className="searchIcon">
            {/* {filteredData && <SearchIcon />} */}
            {/* {!flag ? (
              <CloseIcon id="clearBtn" onClick={clearInput} />
            ) : ( */}
            <SearchIcon onClick={() => setCity(wordEntered)} />
            <CloseIcon id="clearBtn" onClick={clearInput} />
            {/* )} */}
          </div>
        </div>
        {filteredData.length != 0 && (
          <div className="dataResult1">
            {filteredData.slice(0, 15).map((value, key) => {
              return (
                <>
                  {flag && (
                    <div className="dataItem" target="_blank">
                      <span onClick={() => handleClick(value.name)}>
                        {value.name}{" "}
                      </span>
                    </div>
                  )}
                </>
              );
            })}
          </div>
        )}
      </div>
    </>
  );
}

export default SearchBar;
