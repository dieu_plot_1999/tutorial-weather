import React from "react";
import {Typography} from "@mui/material";
import moment from "moment";
export default function Addittional({data}) {
  return (
    <>
      <div
        style={{
          lineHeight: "2px",
        }}
      >
        <Typography>MT mộc</Typography>
        <Typography>
          {moment.unix(data?.sys?.sunrise).format("H:mm")}
        </Typography>
        <Typography>Độ ấm</Typography>
        <Typography>{data?.main?.humidity}%</Typography>
      </div>
      <div>
        <Typography>MT lặn</Typography>
        <Typography>{moment.unix(data?.sys?.sunset).format("H:mm")}</Typography>
        <Typography>Gió</Typography>
        <Typography>{(data?.wind?.speed * 3.6).toFixed(2)}km/h</Typography>
      </div>
    </>
  );
}
